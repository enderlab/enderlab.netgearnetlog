#include <FastLED.h>
#include <WiFiManager.h>
#include <ESP_DoubleResetDetector.h>
#include <ESP8266WiFi.h>
//#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>

//debug serial baud rate
#define SERIAL_BAUD 9600
//global LED brightness
#define BRIGHTNESS 40
// seconds waits for wifi to connect
#define WIFI_WAIT_TIME 20
// seconds the config portal waits before resetting
#define PORTAL_WAIT_TIME 180
#define FRIENDLY_NAME "EnderRxTx"
#define PORTAL_NAME "Ender Lab Rx Tx Meter"
//socket server port
#define LISTEN_PORT 15612
//unused. planned to be the time for a dot to travel the strip
#define STRIP_TRAVEL_MS 1000
//LED count on each strip
#define LED_COUNT 120
//the offstrip buffer size on which the current packet is drawn
//MUST BE A MULTIPLE OF LED_COUNT
#define STRIP_BUFFER 20

WiFiManager wifiManager;
#define PIN_RX D3
#define PIN_TX D4

#define LED_ARRAY_SIZE (LED_COUNT * 2)
//rx led controller
CLEDController *controller_rx;
//rx receive window
CRGB leds_rx_underlying[LED_ARRAY_SIZE];
//current rx pixel
CRGB *leds_rx;
//tx led controller
CLEDController *controller_tx;
//tx receive window
CRGB leds_tx_underlying[LED_ARRAY_SIZE];
//current tx pixel
CRGB *leds_tx;

//not ideal, but for now
byte rx_live = 0;
byte tx_live = 0;
byte colorRx_live = 0;
byte colorTx_live = 0;
byte lastRxColor = 0;
byte lastTxColor = 0;

unsigned long pause_ms = 0;

WiFiServer wifiServer(LISTEN_PORT);

//#define DEBUG 1

bool connected = false;
WiFiClient client;
byte msg[8];
byte loop_counter = 0;
void loop()
{
    //check status
    if (WiFi.status() != WL_CONNECTED)
    {
        ESP.restart();
        return; //noop
    }

    //OTA updates handling
    //ArduinoOTA.handle();

    //check connection
    checkConnection();

    if (client.connected())
    {
        //read all bytes from the line and update associated vars
        readClient();

        //each iteration, draw current window
        leds_rx = &leds_rx_underlying[loop_counter];
        controller_rx->setLeds(leds_rx, LED_COUNT);
        leds_tx = &leds_tx_underlying[LED_COUNT - loop_counter];
        controller_tx->setLeds(leds_tx, LED_COUNT);

        //draw the strip
        FastLED.show();

        //every STRIP_BUFFER output, draw next "frame" on the STRIP_BUFFER area
        if (loop_counter % STRIP_BUFFER == 0)
        {
            drawNextStripBuffer();
        }

        loop_counter++;

        //copy last LED of new frame to first led minus 1
        leds_rx_underlying[loop_counter - 1] = leds_rx_underlying[LED_COUNT - 1 + loop_counter];
        leds_tx_underlying[LED_ARRAY_SIZE - loop_counter] = leds_tx_underlying[LED_COUNT - loop_counter];

        if (loop_counter == LED_COUNT)
            loop_counter = 0;
    }
    else
    {
        allOff(true);
    }

    //loop delay
    delay(pause_ms);
}

void checkConnection()
{
    if (!client.connected())
    {
        if (connected)
        {
            connected = false;
#ifdef DEBUG
            Serial.println("disconnection");
#endif
        }
        client = wifiServer.available();
    }
}

void drawNextStripBuffer()
{
    CRGB *draw_rx = &leds_rx[LED_COUNT];
    CRGB *draw_tx = &leds_tx[-1];

    //for making color changes a gradient
    float stepRx = (colorRx_live - lastRxColor) / STRIP_BUFFER;
    float stepTx = (colorTx_live - lastTxColor) / STRIP_BUFFER;

    for (byte i = 0; i < STRIP_BUFFER; i++)
    {
        byte r = random(0, 255);
        if (rx_live == 255 || r < rx_live)
        {
            lastRxColor += stepRx;
            draw_rx[i] = CHSV(lastRxColor, 255, 255);
        } else {
            draw_rx[i] = CRGB::Black;
        }
        r = random(0, 255);
        if (tx_live == 255 || r < tx_live)
        {
            lastTxColor += stepTx;
            draw_tx[-i] = CHSV(lastTxColor, 255, 255);
        } else {
            draw_tx[-i] = CRGB::Black;
        }
    }
    //for using next frame as the "from" gradient
    lastRxColor = colorRx_live;
    lastTxColor = colorTx_live;
}

void readClient()
{
    if (!connected)
    {
        connected = true;
#ifdef DEBUG
        Serial.println("connection");
#endif
    }
    while (client.available() > 7)
    {
        client.read(msg, 8);
        //be sure to end on \r\n
        if (msg[6] != '\r' || msg[7] != '\n')
        {
            while (true)
            {
                while (client.available() < 2)
                    ; //semi-colon here
                msg[6] = msg[7];
                msg[7] = client.read();
                if (msg[6] == '\r' && msg[7] == '\n')
                    break;
            }
            continue;
        }
#ifdef DEBUG
        Serial.write(msg[0]);
        Serial.write(msg[1]);
        Serial.write(msg[2]);
        Serial.write(msg[3]);
        Serial.write(msg[4]);
        Serial.write(msg[5]);
        Serial.println();
#endif
        processCommand(msg[0], msg[1], msg[2], msg[3], msg[4], msg[5]);
    }
}

void processCommand(byte speedRx, byte speedTx, byte colorRx, byte colorTx, byte rx, byte tx)
{
#ifdef DEBUG
    //Serial.println(rx);
#endif

    //process the bytes
    rx_live = rx;
    tx_live = tx;

    colorRx_live = colorRx;
    colorTx_live = colorTx;
}

//everything below here is standard setup of stuffs
void setup()
{
#ifdef DEBUG
    Serial.begin(SERIAL_BAUD);
    Serial.setDebugOutput(true);
    Serial.println("setup begin");
#endif

    WiFi.hostname(FRIENDLY_NAME);
    //ArduinoOTA.setHostname(FRIENDLY_NAME);

    /*
#ifdef DEBUG
    ArduinoOTA.onStart([]() {
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
#endif
*/

    //setup rx leds
    pinMode(PIN_RX, OUTPUT);
    //rx starts at bottom and moves up
    leds_rx = &leds_rx_underlying[0];
    controller_rx = &FastLED.addLeds<WS2812, PIN_RX, GRB>(leds_rx, LED_COUNT).setCorrection(TypicalLEDStrip);

    //setup tx leds
    pinMode(PIN_TX, OUTPUT);
    //tx starts at top and moves down, but for init start at bottom
    leds_tx = &leds_tx_underlying[0];
    controller_tx = &FastLED.addLeds<WS2812, PIN_TX, GRB>(leds_tx, LED_COUNT).setCorrection(TypicalLEDStrip);

    //global brightnesss
    FastLED.setBrightness(BRIGHTNESS);

    allOff(true);

    int8_t lightOffset = (LED_COUNT - 1) / 5;
    leds_rx[0 * lightOffset] = CRGB::Blue;
    leds_rx[1 * lightOffset] = CRGB::Blue;
    leds_rx[2 * lightOffset] = CRGB::Blue;
    leds_rx[3 * lightOffset] = CRGB::Blue;
    leds_rx[4 * lightOffset] = CRGB::Blue;
    leds_rx[5 * lightOffset] = CRGB::Blue;
    leds_tx[0 * lightOffset] = CRGB::Blue;
    leds_tx[1 * lightOffset] = CRGB::Blue;
    leds_tx[2 * lightOffset] = CRGB::Blue;
    leds_tx[3 * lightOffset] = CRGB::Blue;
    leds_tx[4 * lightOffset] = CRGB::Blue;
    leds_tx[5 * lightOffset] = CRGB::Blue;
    FastLED.show();
    FastLED.delay(1000);

    //reset settings - for testing
    //wifiManager.resetSettings();

    //shows alternating blue/yellow while connecting, solid yellow for AP config, green for connected
    connect();

    //ArduinoOTA.begin(true);

#ifdef DEBUG
    Serial.println("setup complete");
#endif

    allOff();

    //set the tx to the middle
    leds_tx = &leds_tx_underlying[LED_COUNT];
    controller_tx->setLeds(leds_tx, LED_COUNT);
}

void configureWifi()
{
    allOff(true);
    int8_t lightOffset = (LED_COUNT - 1) / 5;
    leds_rx[0 * lightOffset] = CRGB::Yellow;
    leds_rx[1 * lightOffset] = CRGB::Yellow;
    leds_rx[2 * lightOffset] = CRGB::Yellow;
    leds_rx[3 * lightOffset] = CRGB::Yellow;
    leds_rx[4 * lightOffset] = CRGB::Yellow;
    leds_rx[5 * lightOffset] = CRGB::Yellow;
    leds_tx[0 * lightOffset] = CRGB::Yellow;
    leds_tx[1 * lightOffset] = CRGB::Yellow;
    leds_tx[2 * lightOffset] = CRGB::Yellow;
    leds_tx[3 * lightOffset] = CRGB::Yellow;
    leds_tx[4 * lightOffset] = CRGB::Yellow;
    leds_tx[5 * lightOffset] = CRGB::Yellow;
    FastLED.show();
    FastLED.delay(100);

    wifiManager.setConfigPortalTimeout(PORTAL_WAIT_TIME);
    wifiManager.startConfigPortal(PORTAL_NAME);

    allOff();
    ESP.restart();
}

void connect()
{
    int8_t lightOffset = (LED_COUNT - 1) / 5;

    while (WiFi.status() != WL_CONNECTED)
    {
        int8_t waitCount = 0;
        while (WiFi.status() != WL_CONNECTED && waitCount < WIFI_WAIT_TIME)
        {
            leds_rx[0 * lightOffset] = CRGB::Blue;
            leds_rx[1 * lightOffset] = CRGB::Yellow;
            leds_rx[2 * lightOffset] = CRGB::Blue;
            leds_rx[3 * lightOffset] = CRGB::Yellow;
            leds_rx[4 * lightOffset] = CRGB::Blue;
            leds_rx[5 * lightOffset] = CRGB::Yellow;
            leds_tx[0 * lightOffset] = CRGB::Blue;
            leds_tx[1 * lightOffset] = CRGB::Yellow;
            leds_tx[2 * lightOffset] = CRGB::Blue;
            leds_tx[3 * lightOffset] = CRGB::Yellow;
            leds_tx[4 * lightOffset] = CRGB::Blue;
            leds_tx[5 * lightOffset] = CRGB::Yellow;
            FastLED.show();
            delay(500);
            leds_rx[0 * lightOffset] = CRGB::Yellow;
            leds_rx[1 * lightOffset] = CRGB::Blue;
            leds_rx[2 * lightOffset] = CRGB::Yellow;
            leds_rx[3 * lightOffset] = CRGB::Blue;
            leds_rx[4 * lightOffset] = CRGB::Yellow;
            leds_rx[5 * lightOffset] = CRGB::Blue;
            leds_tx[0 * lightOffset] = CRGB::Yellow;
            leds_tx[1 * lightOffset] = CRGB::Blue;
            leds_tx[2 * lightOffset] = CRGB::Yellow;
            leds_tx[3 * lightOffset] = CRGB::Blue;
            leds_tx[4 * lightOffset] = CRGB::Yellow;
            leds_tx[5 * lightOffset] = CRGB::Blue;
            FastLED.show();
            delay(500);

            waitCount++;
        }

        if (WiFi.status() != WL_CONNECTED)
        {
            configureWifi();
        }
    }

    //lights are still all green at this point
    startServer();

    allOff();
    delay(100);
    for (uint8_t i = 0; i < 6; i++)
    {
        leds_rx[i * lightOffset] = CRGB::Green;
        leds_tx[i * lightOffset] = CRGB::Green;
        FastLED.show();
        delay(500);
    }
    delay(1000);
    allOff();
}

void startServer()
{
    wifiServer.begin();
}

void allOff()
{
    allOff(true);
}

void allOff(bool show)
{
    for (int i = 0; i < LED_ARRAY_SIZE; i++)
    {
        leds_rx_underlying[i] = CRGB::Black;
        leds_tx_underlying[i] = CRGB::Black;
    }
    if (show)
        FastLED.show();
}

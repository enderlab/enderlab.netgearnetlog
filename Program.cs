﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using PrimS.Telnet;

namespace EnderLab.NetgearNetLog
{
    /// <summary>
    /// This is the .NET "server" side of the rxtx lights.  It reads the total bytes via telnet into the router, does some math,
    /// then sends a 6 byte control packet to an esp8266 to direct on how to show lights  
    /// The packet has 2 bytes for speed, 2 for color, and 2 for density.
    /// Speed - unused
    /// Color - based on delta of kbps from previous frame, a % of total
    /// Density - based on current kbps, a % of total
    /// </summary>
    class Program
    {
        //ms per packet sent to device
        private const double PRECISION = 300;
        //the top stop to use when computing the data rate.
        //data kbps above these are considered 100%
        private static readonly double CAP_RX = double.Parse(Environment.GetEnvironmentVariable("CAP_RX") ?? "400000");
        private static readonly double CAP_TX = double.Parse(Environment.GetEnvironmentVariable("CAP_TX") ?? "25000");
        //values below this (in kbps) are considered 0
        private static readonly double BOTTOM_RX = double.Parse(Environment.GetEnvironmentVariable("BOTTOM_RX") ?? "15");
        private static readonly double BOTTOM_TX = double.Parse(Environment.GetEnvironmentVariable("BOTTOM_TX") ?? "5");
        //for determining the data rate, we use a reverse exponential whereby small changes on the lower area are amplified.
        //closer to 1 becomes linear. ex. with "2" we get the sqrt
        //50kbps would be 50^(1/2) / 25000^(1/2) or 7/158 or about 5%.
        //500kbps would be 500^(1/2) / 25000^(1/2) or 22/158 or about 15%.
        //5000kbps would be 5000^(1/2) / 25000^(1/2) or 70/158 or about 44%. (linearly 5k is only 20% of 25k)
        private static readonly double RX_ROOT = double.Parse(Environment.GetEnvironmentVariable("RX_ROOT") ?? "2");
        private static readonly double TX_ROOT = double.Parse(Environment.GetEnvironmentVariable("TX_ROOT") ?? "2");
        //offset from 0 to use for colors (0 is red, color wheel 8 bits back to red.  google fastled color hue)
        private static readonly short COLOR_RX_OFFSET = short.Parse(Environment.GetEnvironmentVariable("COLOR_RX_OFFSET") ?? "6");
        private static readonly short COLOR_TX_OFFSET = short.Parse(Environment.GetEnvironmentVariable("COLOR_TX_OFFSET") ?? "98");
        //similar to above, the color is determined by the delta in kbps from prev kbps as a % of range, with a expoential boost like above.
        private static readonly double DELTA_RX_ROOT = double.Parse(Environment.GetEnvironmentVariable("DELTA_RX_ROOT") ?? "1.3");
        private static readonly double DELTA_TX_ROOT = double.Parse(Environment.GetEnvironmentVariable("DELTA_TX_ROOT") ?? "1.5");
        //color is chosen from the nth root of the change in bandwidth %. This calms that down a bit further linearly to prevent massive color changes
        private static readonly short RX_DELTA_DIVISOR = short.Parse(Environment.GetEnvironmentVariable("RX_DELTA_DIVISOR") ?? "4");
        private static readonly short TX_DELTA_DIVISOR = short.Parse(Environment.GetEnvironmentVariable("TX_DELTA_DIVISOR") ?? "2");

        //timeout val
        private static readonly TimeSpan _s_ts = new TimeSpan(0, 0, 15);
        //the regexes to use during ifconfig
        private static readonly Regex _s_regEx = new Regex(".*bytes:([0-9.]*).+\\(.+\\).+TX bytes:([0-9.]*).*");
        private static readonly Regex _s_regExInterface = new Regex("([\\w\\d]+)\\s");
        private static readonly Stopwatch _s_sw = new Stopwatch();

        //used by ifconfig
        //originally code supported multiple interfaces
        //and still could by blanking this and handling different values in the list returned by GetInfo
        private static readonly string _s_interface = Environment.GetEnvironmentVariable("INTERFACE") ?? "ethwan";

        static async Task Main(string[] args)
        {
            // examples from ifconfig on my router
            // ath0 ath01 ath1 ath11 ethlan ethwan
            //RX bytes:173485430824 (161.5 GiB)  TX bytes:17931648550 (16.6 GiB)

            //compute once
            var rtCapRx = Math.Pow(CAP_RX, 1 / RX_ROOT);
            var rtCapTx = Math.Pow(CAP_TX, 1 / TX_ROOT);
            var rtCapDeltaRx = Math.Pow(CAP_RX, 1 / DELTA_RX_ROOT);
            var rtCapDeltaTx = Math.Pow(CAP_TX, 1 / DELTA_TX_ROOT);

            Console.WriteLine($"rtCapRx is {rtCapRx}");
            Console.WriteLine($"rtCapTx is {rtCapTx}");
            Console.WriteLine($"rtCapDeltaRx is {rtCapDeltaRx}");
            Console.WriteLine($"rtCapDeltaTx is {rtCapDeltaTx}");

            //this uses a telnet client to netgear nighthawk router
            using (Client client = new Client(Environment.GetEnvironmentVariable("ROUTER_ADDR"), 23, new System.Threading.CancellationToken()))
                //unchecked to allow bytes to wrap. ex, -1 = 255, -2 = 254
                unchecked
                {
                    var pw = Environment.GetEnvironmentVariable("PASSWORD");
                    await client.TerminatedReadAsync("password:", _s_ts);
                    await client.WriteLine(pw);
                    await client.TerminatedReadAsync("/#", _s_ts);
                    var all = await GetInfo(client);

                    Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sender.Connect(Environment.GetEnvironmentVariable("ESP_ADDR"), 15612);
                    var buf = new byte[8];
                    //last 2 bytes ALWAYS \r\n and validated by client.
                    buf[6] = (byte)'\r';
                    buf[7] = (byte)'\n';

                    double lastDrx = 0;
                    double lastDtx = 0;
                    while (true)
                    {
                        _s_sw.Restart();
                        //always just 1 row now
                        var newAll = await GetInfo(client);
                        for (int i = 0; i < newAll.Length; i++)
                        {
                            all[i].drx = (newAll[i].rx - all[i].rx) / PRECISION;
                            all[i].dtx = (newAll[i].tx - all[i].tx) / PRECISION;
                            all[i].rx = newAll[i].rx;
                            all[i].tx = newAll[i].tx;

                            //speed not implemented on either side
                            buf[0] = 0x00;
                            buf[1] = 0x00;

                            //color rx
                            var delta = all[i].drx - lastDrx;
                            int negative = delta < 0 ? -1 : 1;
                            delta *= negative;
                            buf[2] = (byte)((byte)(delta >= CAP_RX ? 255 : (byte)(((Math.Pow(delta, 1 / DELTA_RX_ROOT) / rtCapDeltaRx) * 255 * negative) / RX_DELTA_DIVISOR)) + COLOR_RX_OFFSET);

                            //color tx
                            delta = all[i].dtx - lastDtx;
                            negative = delta < 0 ? -1 : 1;
                            delta *= negative;
                            buf[3] = (byte)((byte)(delta >= CAP_TX ? 255 : (byte)(((Math.Pow(delta, 1 / DELTA_TX_ROOT) / rtCapDeltaTx) * 255 * negative) / TX_DELTA_DIVISOR)) + COLOR_TX_OFFSET);

                            //rx
                            double crx = all[i].drx - BOTTOM_RX;
                            if (crx < 0) crx = 0;
                            buf[4] = (byte)(crx >= CAP_RX ? 255 : (byte)((Math.Pow(crx, 1 / RX_ROOT) / rtCapRx) * 255));

                            //tx
                            double ctx = all[i].dtx - BOTTOM_TX;
                            if (ctx < 0) ctx = 0;
                            buf[5] = (byte)(ctx >= CAP_TX ? 255 : (byte)((Math.Pow(ctx, 1 / TX_ROOT) / rtCapTx) * 255));

                            //send the control signal to the esp8266
                            sender.Send(buf);
                            //Console.WriteLine($"{all[i].i} speed is \t{all[i].drx,0:0}kB/s down \t\t {all[i].dtx,0:0}kB/s up");
                            //Console.WriteLine($"buf is {buf[0]} {buf[1]} {buf[2]} {buf[3]} {buf[4]} {buf[5]}");

                            lastDrx = all[i].drx;
                            lastDtx = all[i].dtx;
                        }
                        //compute time to sleep based on how long it took to run the above
                        int elapsed = (int)PRECISION - (int)_s_sw.ElapsedMilliseconds;
                        Thread.Sleep(elapsed < 0 ? 0 : elapsed);
                    }
                }
        }

        private static async Task<(string i, double rx, double tx, double drx, double dtx)[]> GetInfo(Client client)
        {
            //could run with no interface and get all if wanted analysis across more
            await client.WriteLine($"ifconfig {_s_interface}");
            var s = await client.TerminatedReadAsync("/#", _s_ts);
            var interfaces = s.Split("\r\n\r\n", StringSplitOptions.RemoveEmptyEntries);
            var all1 = interfaces.Select(a => a.Split("\r\n").Where(a => !a.StartsWith("ifconfig") && !a.StartsWith("root") && !a.TrimStart().StartsWith("Interrupt"))).Where(a => a.Count() > 2);
            var all2 = all1.Select(a => (i: _s_regExInterface.Matches(a.First())[0].Groups[1].Value, r: _s_regEx.Matches(a.Last()))).Where(a => a.r.Count > 0);
            var all3 = all2.Select(a => (i: a.i, rx: double.Parse(a.r[0].Groups[1].Value), tx: double.Parse(a.r[0].Groups[2].Value), drx: (double)0, dtx: (double)0));
            var all4 = all3.ToArray();
            return all4;
        }
    }
}
